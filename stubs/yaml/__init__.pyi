from typing import Union, Dict, Hashable, Any, Optional, Type, TextIO

# noinspection PyPackageRequirements
from .dumper import *
# noinspection PyPackageRequirements
from .error import *
# noinspection PyPackageRequirements
from .events import *
# noinspection PyPackageRequirements
from .loader import *
# noinspection PyPackageRequirements
from .nodes import *
# noinspection PyPackageRequirements
from .tokens import *


def load(stream: Any, Loader: Any = None) -> Union[Dict[Hashable, Any]]: ...


def safe_load(stream: Any) -> Union[Dict[Hashable, Any]]: ...


def dump(data: Any, stream: Optional[TextIO] = None, Dumper: Type[Dumper] = Dumper, **kwds: Any) -> Optional[str]: ...
