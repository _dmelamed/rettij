from typing import Any

def loads(
    s: Any, base_uri: str = "", loader: Any = None, jsonschema: bool = False, load_on_repr: bool = True, **kwargs: Any
) -> dict: ...
