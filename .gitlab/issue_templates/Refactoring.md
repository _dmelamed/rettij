## Refactoring Proposal
<!---
Please read this!

This template is meant for proposals for structural changes (code refinement, name changes) to rettij.
For NEW features, please use the "Feature Proposal" template.

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "refactoring" label (https://gitlab.com/frihsb/rettij/-/issues?label_name%5B%5D=refactoring)
and verify the issue you're about to submit isn't a duplicate.
--->

### Improvement to make

<!-- What is the improvement you are trying to solve make this issue? -->

### Proposal 

<!-- Use this section to explain the refactoring and how it will change the code base. It can be helpful to add technical details, design proposals, and links to related epics or issues. -->

/label ~refactoring