class ExecutorNotAvailableException(Exception):
    """
    Exception thrown if the container is not running.
    """

    pass
