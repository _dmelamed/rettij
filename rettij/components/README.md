This directory contains the standard components shipped with rettij.

Note for developers: When updating a component image version, do not forget to update the image tag in the specification file as well.
