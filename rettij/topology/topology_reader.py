import importlib.util
from importlib.abc import Loader
from ipaddress import IPv4Interface
from pathlib import Path
from types import ModuleType
from typing import Dict, Tuple, Union, Type, List, Any, Optional

# This import works, but PyCharm keeps flagging it as "Unresolved reference"
# noinspection PyUnresolvedReferences
from importlib._bootstrap import ModuleSpec

import yaml
from rettij.common.constants import USER_DIR, COMPONENTS_DIR
from rettij.common.logging_utilities import LoggingSetup
from rettij.common.validated_path import ValidatedFilePath, ValidatedDirPath
from rettij.common.yaml_loader_with_line_numbers import YamlLoaderWithLineNumbers
from rettij.exceptions.invalid_path_exception import InvalidPathException
from rettij.exceptions.topology_exception import TopologyException
from rettij.topology.mac_address_pool import MacAddressPool
from rettij.topology.network_components.channel import Channel
from rettij.topology.network_components.interface import Interface, ExternalSettingsStore
from rettij.topology.network_components.node import Node
from rettij.topology.network_components.route import Route
from rettij.topology.node_configurations.host_config import HostConfig
from rettij.topology.node_configurations.kubernetes_pod_config import KubernetesPodConfig
from rettij.topology.node_configurations.node_config import NodeConfig
from rettij.topology.node_container import NodeContainer
from rettij.topology.node_executors.host_executor import Host
from rettij.topology.node_executors.kubernetes_pod_executor import KubernetesPodExecutor
from rettij.topology.node_executors.node_executor import NodeExecutor
from rettij.topology.validators.validator_executor import ValidatorExecutor


class TopologyReader:
    """
    This class is responsible for parsing the topology file.
    """

    MAJOR_VERSION: str = "1"
    MINOR_VERSION: str = "1"

    def __init__(self) -> None:
        """
        Initialize a TopologyReader object.
        """
        self.topology: Dict = {}
        self.logger = LoggingSetup.submodule_logging(self.__class__.__name__)

        self.channel_vni = 0
        self.channels: Dict[str, Channel] = {}  # {id: channel}

    def read(
        self,
        topology_file_path: ValidatedFilePath,
        components_dir_path: ValidatedDirPath = ValidatedDirPath(COMPONENTS_DIR),
    ) -> Tuple[NodeContainer, Dict[str, Channel]]:
        """
        Parse the rettij topology.

        Validates and reads the topology file and return Nodes and Channels.

        :param topology_file_path: Absolute path to the topology file
        :param components_dir_path: (Optional) Absolute path to the custom components' directory
        :return: Nodes and Channels with their names
        :raises: InvalidPathException: If a file or directory path is invalid
        """
        nodes: NodeContainer = NodeContainer()  # {id: node}
        self.channels = {}  # make sure the channels dict is empty (may contain artifacts from earlier usage)

        self.logger.debug(
            f"Reading topology file '{topology_file_path}' with components directory '{components_dir_path}'..."
        )

        # Load and validate the topology
        self.topology = TopologyReader.load_yaml(topology_file_path)
        ValidatorExecutor(self.topology, topology_file_path, self.MAJOR_VERSION, self.MINOR_VERSION).run_validations()

        # Load all explicitly defined channels
        for c in self.topology.get("channels", []):
            self._add_channel(c["id"], c.get("data-rate"), c.get("delay"))

        # Load all nodes
        for node_yaml in self.topology["nodes"]:
            # Store the node
            node: Node = self._create_node(node_yaml, components_dir_path)
            nodes[node.name] = node

        # Reset node yaml specification
        node_yaml = {}

        # Verify that all implicitly created channels are valid point-to-point connections (exactly 2 nodes connected)
        # Explicitly declared channels were already verified by ChannelValidator._check_channel_references()
        for channel_id, channel in self.channels.items():
            connected_nodes_count: int = len(channel.connected_node_names_and_data_rates.keys())
            if connected_nodes_count != 2:
                self.logger.warning(
                    f"Autogenerated Channel {channel_id} has {connected_nodes_count} connection(s) instead of 2! "
                    f"Connected nodes: {', '.join(channel.connected_node_names_and_data_rates.keys())}"
                )

        return nodes, self.channels

    def _create_node(self, node_yaml: Dict, components_dir_path: ValidatedDirPath) -> Node:
        """
        Create a single simulation Node from a yaml specification.

        :param node_yaml: Yaml specification as dictionary.
        :param components_dir_path: Absolute path to the custom components' directory
        :return: Newly created Node.
        """
        # Create the Node
        device_type: str = node_yaml["device"]
        node_id: str = node_yaml["id"]
        executor_type: Type[NodeExecutor]
        config: NodeConfig

        if device_type == "vm":
            raise NotImplementedError()
        elif device_type == "physical":
            raise NotImplementedError()
        elif device_type == "host":
            executor_type = Host
        else:
            executor_type = KubernetesPodExecutor

        # Load the component
        component: Union[str, dict] = node_yaml["component"]

        # Load the component base path
        component_base_path: Optional[ValidatedDirPath] = None
        if isinstance(component, str):
            try:
                component_base_path = ValidatedDirPath.join_paths(components_dir_path, component)
            except InvalidPathException as e1:
                try:
                    component_base_path = ValidatedDirPath.join_paths(COMPONENTS_DIR, component)
                except InvalidPathException as e2:
                    raise ValueError(
                        f"No component found for service '{component}' at either '{str(e1.path_normalized)}' or '{str(e2.path_normalized)}"
                    )

        # Load the node data if one is supplied.
        datadict: dict = {}
        try:
            datadict = node_yaml["data"]
            # Remove the '__line__' entry used for better error messages during yaml parsing
            datadict.pop("__line__")
        except KeyError:
            pass

        # Initialize hooks dictionary
        hooks: Dict[str, List[Any]] = {
            "pre-deploy": [],
            "post-deploy": [],
            "connect": [],
            "post-connect": [],
            "pre-teardown": [],
        }

        if component_base_path:
            # Load all hooks
            self._load_component_hook("pre-deploy", "PreDeployHook", component_base_path, hooks)
            self._load_component_hook("post-deploy", "PostDeployHook", component_base_path, hooks)
            self._load_component_hook("connect", "ConnectHook", component_base_path, hooks)
            self._load_component_hook("post-connect", "PostConnectHook", component_base_path, hooks)
            self._load_component_hook("pre-teardown", "PreTeardownHook", component_base_path, hooks)

        # Load custom node configuration directory, if one is supplied
        config_dir: Union[Path, None] = None
        if "config-dir" in node_yaml.get("config", []):
            config_dir = (Path(USER_DIR) / "config" / node_yaml["config"]["config-dir"]).resolve()

        if executor_type == KubernetesPodExecutor:
            pod_spec: Dict

            if component_base_path:
                assert isinstance(component, str)
                # Ensures that a specification file for that service exists
                pod_spec_file: ValidatedFilePath
                try:
                    pod_spec_file = ValidatedFilePath.join_paths(components_dir_path, component, f"{component}.yaml")
                except InvalidPathException as e1:
                    try:
                        pod_spec_file = ValidatedFilePath.join_paths(COMPONENTS_DIR, component, f"{component}.yaml")
                    except InvalidPathException as e2:
                        raise ValueError(
                            f"No k8s container spec file found for Service '{component}' at either '{str(e1.path_normalized)}' or '{str(e2.path_normalized)}"
                        )
                with open(pod_spec_file, "r") as fd:
                    pod_spec = yaml.safe_load(fd)
            else:
                assert isinstance(component, Dict)
                container_spec: Dict = component
                image: str = container_spec["image"]
                # If no name is supplied, extract from image (i.e. 'rettij_simple-runner' from 'frihsb/rettij_simple-runner:latest')
                # "A tag name must be valid ASCII and may contain lowercase and uppercase letters, digits, underscores, periods and dashes. A tag name may not start with a period or a dash and may contain a maximum of 128 characters."
                # We need to convert this into valid DNS names, which means replacing '_' with '-' and stripping all special characters at the start or end of the string.
                if not container_spec.get("name"):
                    # Strip the image path prefix (e.g. 'frihsb/')
                    # If no path prefix is present, the container name will begin the same as the image name.
                    try:
                        index_name_start = image.rindex("/") + 1
                    except ValueError:
                        index_name_start = 0

                    # Strip the image tag (e.g. ':latest')
                    # If no tag is present, the container name will end the same as the image name.
                    try:
                        index_name_end = image.rindex(":")
                    except ValueError:
                        index_name_end = len(image) + 1

                    container_spec["name"] = image[index_name_start:index_name_end].replace("_", "-").strip("-_.")

                container_spec.pop("__line__")
                pod_spec = {"containers": [container_spec]}

            # Load execution host
            # If no execution host ist supplied, Kubernetes will decide for itself
            execution_host: str = ""
            if "execution-host" in node_yaml.get("config", []):
                execution_host = node_yaml["config"]["execution-host"]

            # Create the Node configuration
            config = KubernetesPodConfig(pod_spec, hooks, execution_host, config_dir)

        elif executor_type == Host:
            config = HostConfig(hooks)

        else:
            raise NotImplementedError()

        node = Node(executor_type, node_id, device_type, config=config, data=datadict)

        # Create the Interfaces for the Node
        for iface in node_yaml.get("interfaces", []):

            try:
                # Load existing channel if it was explicitly defined
                channel = self.channels[iface["channel"]]
            except KeyError:
                # Create and store new channel
                channel = self._add_channel(iface["channel"])

            if device_type in ["switch", "hub"]:
                iface = Interface(
                    node.executor,
                    iface["id"],
                    channel,
                    iface["mac"] if "mac" in iface else MacAddressPool.generate_unique_mac(),
                    data_rate=iface.get("data-rate"),
                )
            # New variant of defining a host bridge interface
            elif device_type == "host" and isinstance(node.config, HostConfig) and iface.get("external"):
                iface = Interface(
                    node.executor,
                    iface["id"],
                    channel,
                    iface["mac"] if "mac" in iface else MacAddressPool.generate_unique_mac(),
                    data_rate=iface.get("data-rate"),
                    external=ExternalSettingsStore(iface["external"]["interface"], iface["external"]["networks"]),
                )
            # TODO: Remove with the next major version increase of the topology file format
            # Old variant of defining a host bridge interface
            #   id: n0
            #   interfaces:
            #     - id: i0
            #       channel: c0
            #   config:
            #     is-bridge: true
            #     external-networks:
            #       - 10.0.0.0/24
            #     external-interface: eth1
            elif (
                device_type == "host"
                and isinstance(node.config, HostConfig)
                and node_yaml.get("config")
                and node_yaml["config"].get("is-bridge") is True
            ):
                iface = Interface(
                    node.executor,
                    iface["id"],
                    channel,
                    iface["mac"] if "mac" in iface else MacAddressPool.generate_unique_mac(),
                    data_rate=iface.get("data-rate"),
                    external=ExternalSettingsStore(
                        node_yaml["config"]["external-interface"], node_yaml["config"]["external-networks"]
                    ),
                )
            else:
                iface = Interface(
                    node.executor,
                    iface["id"],
                    channel,
                    iface["mac"] if "mac" in iface else MacAddressPool.generate_unique_mac(),
                    IPv4Interface(iface["ip"]),
                    data_rate=iface.get("data-rate"),
                )
            node.add_interface(channel, iface)

        # Read and save the routes for the Node
        if node_yaml.get("routes", []):
            for route_yml in node_yaml.get("routes", []):
                route = Route(
                    route_yml["network"],
                    route_yml["gateway"],
                    route_yml["metric"] if ("metric" in route_yml) else None,
                )
                node.routes.append(route)

        return node

    def _load_component_hook(
        self, hook_name: str, hook_class_name: str, component_base_path: ValidatedDirPath, hooks: Dict[str, List[Any]]
    ) -> None:
        """
        Load a specific type of component hook.

        :param hook_name: Name of the hook
        :param hook_class_name: Class name for the hook.
            Options:
            - PreDeployHook
            - PostDeployHook
            - ConnectHook
            - PostConnectHook
            - PreTeardownHook

        :param component_base_path: Base path of the component
        :param hooks: Dictionary of hooks for the current Node
        """
        # TODO: See if there is a more elegant way to do this
        try:
            hook_path: ValidatedFilePath = ValidatedFilePath.join_paths(component_base_path, "hooks", f"{hook_name}.py")

            # Load the spec defined in the file
            spec: ModuleSpec = importlib.util.spec_from_file_location(hook_name, str(hook_path))

            # Create a Python module from the spec
            hook_module: ModuleType = importlib.util.module_from_spec(spec)
            if isinstance(spec.loader, Loader):
                spec_loader: Loader = spec.loader
            else:
                raise RuntimeError(f"ModuleSpec {spec.name} does not have a loader!")
            spec_loader.exec_module(hook_module)

            # Retrieve the actual hook class from the new module by its class name
            hook_class: Type[object] = hook_module.__getattribute__(hook_class_name)

            # Instantiate the hook with the new class
            hook: object = hook_class()

            if not isinstance(hook, hook_class):
                raise AttributeError(f"Hook {hook_name} is not of class {hook_class_name}.")

            hooks[hook_name] = [hook]
            self.logger.debug(f"Hook {hook_name} loaded from '{hook_path}'.")

        except InvalidPathException as e:
            # Simply move on if no post-deploy hook is defined
            self.logger.debug(f"No {hook_name} hook found at '{e.path_normalized}'")

    def _add_channel(self, channel_id: str, data_rate: Optional[str] = None, delay: Optional[str] = None) -> Channel:
        """
        Create a new channel and add it to the channel store.

        Automatically assigns an incremental VNI to the newly created channel.

        :param channel_id: Identifier / Name for the channel.
        :param data_rate: (Optional) The virtual bandwidth for the channel. Default: 1Gbps.
        :param delay: (Optional) Virtual delay for the channel. Default: 2ms.
        :return: The newly created Channel.
        """
        channel = Channel(channel_id, self.channel_vni, data_rate, delay)
        self.channels[channel.name] = channel
        self.channel_vni += 1
        return channel

    @staticmethod
    def load_yaml(topology_file_path: str) -> Dict:
        """
        Load a (topology) YAML file with error handling.

        :param topology_file_path: Path to the topology file.
        :return: The parsed YAMl as Dict.
        """
        try:
            with open(ValidatedFilePath(topology_file_path), "r") as fd:
                return yaml.load(fd, Loader=YamlLoaderWithLineNumbers)
        except InvalidPathException as e:
            raise e
        except Exception as e:
            raise TopologyException(
                cause_nbr=TopologyException.DATA_INVALID, topology_file_path=topology_file_path, message=str(e)
            )
