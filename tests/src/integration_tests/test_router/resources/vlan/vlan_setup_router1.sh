##################################################
# From https://wiki.archlinux.org/index.php/VLAN #
##################################################

# Remove IP address from i0
ip addr flush i0

# Add subinterfaces
ip link add link i0 name i0.10 type vlan id 10
ip link add link i0 name i0.20 type vlan id 20

# Add ips to the subinterfaces
ip addr add 10.1.10.1/24 dev i0.10
ip addr add 10.1.20.1/24 dev i0.20

# Bring subinterfaces up
ip link set dev i0.10 up
ip link set dev i0.20 up