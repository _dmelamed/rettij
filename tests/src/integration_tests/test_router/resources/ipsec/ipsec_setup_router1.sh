##################################################################################
# From https://www.tecmint.com/setup-ipsec-vpn-with-strongswan-on-debian-ubuntu/ #
##################################################################################

# Add IPSec tunnel configuration
cp /etc/ipsec.conf /etc/ipsec.conf.BAK
cat <<EOF >> /etc/ipsec.conf

conn site-to-site
      type=tunnel
      auto=start
      keyexchange=ikev2
      authby=secret
      left=192.168.0.2
      leftsubnet=10.1.20.1/24
      right=192.168.0.1
      rightsubnet=10.1.10.2/24
      ike=aes256-sha1-modp1024!
      esp=aes256-sha1!
      aggressive=no
      keyingtries=%forever
      ikelifetime=28800s
      lifetime=3600s
      dpddelay=30s
      dpdtimeout=120s
      dpdaction=restart

EOF

# Add IPSec tunnel PSK (generate a new one for production use!)
cp /etc/ipsec.secrets /etc/ipsec.secrets.BAK
cat <<EOF >> /etc/ipsec.secrets

192.168.0.2 192.168.0.1 : PSK "qLGLTVQOfqvGLsWP75FEtLGtwN3Hu0ku6C5HItKo6ac="

EOF

ipsec start