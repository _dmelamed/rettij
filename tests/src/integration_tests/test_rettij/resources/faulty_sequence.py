from typing import Dict

from rettij.abstract_scheduled_sequence import AbstractScheduledSequence
from rettij.simulation_manager import SimulationManager
from rettij.topology.network_components.channel import Channel
from rettij.topology.node_container import NodeContainer


class ScheduledSequence(AbstractScheduledSequence):
    """
    This class defines a broken scheduled Sequence for testing purposes.
    """

    def define(
        self,
        sm: SimulationManager,
        node: NodeContainer,
        nodes: NodeContainer,
        channel: Dict[str, Channel],
        channels: Dict[str, Channel],
    ) -> None:
        """
        Define the timed steps simulation sequence to be run.

        Add timed steps like this:

        .. code-block:: python

            sm.add_step(
                scheduled_time=1,
                command = nodes.client0.ping,
                args = (nodes.client1.ifaces.i0.ip,),
                kwargs = {'c': 10}
            )

        All parameters are automatically supplied upon execution by rettij.
        :param sm: SimulationManager controlling the simulation.
        :param node: NodeContainer object with all simulation Nodes (same as 'nodes').
        :param nodes: NodeContainer object with all simulation Nodes (same as 'node').
        :param channel: Map of all simulation Channels (same as 'channels').
        :param channels: Map of all simulation Channels (same as 'channel').
        """
        _ = nodes["n3"]  # Node n3 does not exist in the topology
