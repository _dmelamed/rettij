from rettij.topology.hooks.abstract_post_connect_hook import AbstractPostConnectHook
from rettij.topology.network_components.node import Node


class PostConnectHook(AbstractPostConnectHook):
    """
    This class defines a 'post-connect' hook for testing purposes.
    """

    def execute(self, node: Node) -> None:
        """
        Execute the 'post-connect' hook for testing purposes.
        """
        print(f"Post-connect hook of node {node.name}.")
